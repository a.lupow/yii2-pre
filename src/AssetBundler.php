<?php

namespace yii\pre;

use PRE\Component;
use PRE\ResourceBundlerInterface;
use yii\web\AssetBundle;

class AssetBundler extends AssetBundle implements ResourceBundlerInterface {

    /**
     * Current asset bundler instance.
     *
     * @var AssetBundler
     */
    protected static $instance;

    /**
     * Assigns an instance of a bundler to a source of assets.
     *
     * @param AssetBundler $source
     */
    public static function setSource(AssetBundler $source) {
        static::$instance = $source;
    }

    /**
     * A set of actual dependencies.
     * Note that only assets are supported for yii.
     *
     * @var array[]
     */
    protected $dependencies;

    /**
     * {@inheritDoc}
     */
    public static function register($view)
    {
        $assets = static::$instance->getDependencies('assets');
        foreach ($assets as $asset) {
            $asset::register($view);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function addDependency(Component $component, $dependency, $type = 'default')
    {
        $this->dependencies[$type][] = $dependency;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDependencies($type = NULL)
    {
        if (is_null($type)) {
            return $this->dependencies;
        }

        if (empty($this->dependencies[$type])) {
            return [];
        }

        return $this->dependencies[$type];
    }

}

