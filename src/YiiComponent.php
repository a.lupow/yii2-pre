<?php

namespace yii\pre;

use PRE\Component;
use PRE\Environment;
use yii\pre\Exceptions\PreRendererException;

abstract class YiiComponent extends Component {

    /**
     * Component root directory.
     *
     * @var string
     */
    protected $dir;

    /**
     * Template to be rendered.
     *
     * @var string
     */
    public $template;

    public function __construct(Environment $environment)
    {
        parent::__construct($environment);

        // Store component class directory.
        $this->dir = dirname($this->getPath()) . '/';
    }

    /**
     * {@inheritDoc}
     */
    public function registerDependencies()
    {
        if (empty($this->dependencies)) {
            return $this;
        }

        // Add only assets to the dependencies.
        foreach ($this->dependencies as $dependency) {
            $this->addDependency($dependency, 'assets');
        }

        $this->dependencies = [];

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function render($variables = [])
    {
        if (empty($this->template)) {
            throw new PreRendererException(
                printf('No template is assigned to a component %s.', static::class)
            );
        }

        // Pass the template w/ relative path automatically.
        return $this->env->render(
            $this->dir . $this->template, $variables
        );
    }

}
