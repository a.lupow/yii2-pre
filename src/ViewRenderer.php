<?php

namespace yii\pre;

use PRE\Environment;
use PRE\Loaders\FileLoader;
use yii\base\ViewRenderer as BaseViewRenderer;

class ViewRenderer extends BaseViewRenderer {

    /**
     * Rendering environment.
     *
     * @var Environment
     */
    protected $pre;

    /**
     * Cache directories.
     */
    public $cache;

    /**
     * Components namespaces to scan.
     *
     * @var string[]
     */
    public $namespaces;

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $loader = new FileLoader();
        $bundler = new AssetBundler();

        // Initialise namespaces.
        if ($this->namespaces) {
            foreach ($this->namespaces as $namespace) {
                $loader->addRootNamespace($namespace);
            }
        }
        else {
            // Add a default one.
            $loader->addRootNamespace('app\\Components');
        }


        // Initialise the rendering environment.
        $this->pre = new Environment(
            $loader,
            $bundler,
            \Yii::getAlias($this->cache)
        );

        // Set global yii variable.
        $this->pre->addGlobal('app', \Yii::$app);
    }

    /**
     * {@inheritDoc}
     */
    public function render($view, $file, $params)
    {
        // Register the bundler.
        $bundler = new AssetBundler();
        $this->pre->setBundler($bundler);
        AssetBundler::setSource($bundler);

        // Set global vars.
        $this->pre->addGlobal('view', $view);

        // Render out the view.
        return $this->pre->render($file, $params);
    }

}
